let arr = [
    {
        "componentName": "TextField",
        "title": "单行输入框",
        "props": {
            "placeholder": "请输入",
            "label": "付款供应商名称",
            "id": "TextField-4LOTK9TI",
            "required": false,
            "hide": false,
            "edit": false,
            "multiple": false
        },
        "id": "TextField-4LOTK9TI",
        "model": "4LOTK9TI",
        "inTableField": false,
        "aliasLabel": "supplierName",
        "autoWrite": {
            "modelId": "",
            "modelName": "",
            "label": "",
            "key": "",
            "primary": ""
        },
        "orderStateY": "ok",
        "value": "线下供应商1"
    },
    {
        "componentName": "TextField",
        "title": "单行输入框",
        "props": {
            "placeholder": "请输入",
            "label": "付款供应商账户",
            "id": "TextField-FGMEIL93",
            "required": false,
            "hide": false,
            "edit": false,
            "multiple": false
        },
        "id": "TextField-FGMEIL93",
        "model": "FGMEIL93",
        "inTableField": false,
        "aliasLabel": "adverseAccount",
        "autoWrite": {
            "modelId": "",
            "modelName": "",
            "label": "",
            "key": "",
            "primary": ""
        },
        "orderStateY": "ok",
        "value": "中国农业银行  5949494984854"
    },
    {
        "componentName": "TextField",
        "title": "单行输入框",
        "props": {
            "placeholder": "请输入",
            "label": "申请付款金额",
            "id": "TextField-CDXRL2A8",
            "required": false,
            "hide": false,
            "edit": false,
            "multiple": false
        },
        "id": "TextField-CDXRL2A8",
        "model": "CDXRL2A8",
        "inTableField": false,
        "aliasLabel": "money",
        "autoWrite": {
            "modelId": "",
            "modelName": "",
            "label": "",
            "key": "",
            "primary": ""
        },
        "orderStateY": "ok",
        "value": "0.12"
    },
    {
        "componentName": "TextField",
        "title": "单行输入框",
        "props": {
            "placeholder": "请输入",
            "label": "申请最晚付款时间",
            "id": "TextField-4WL2616H",
            "required": false,
            "hide": false,
            "edit": false,
            "multiple": false
        },
        "id": "TextField-4WL2616H",
        "model": "4WL2616H",
        "inTableField": false,
        "aliasLabel": "lastPayTime",
        "autoWrite": {
            "modelId": "",
            "modelName": "",
            "label": "",
            "key": "",
            "primary": ""
        },
        "orderStateY": "ok",
        "value": "2022-04-15 00:00"
    },
    {
        "componentName": "Attachment",
        "title": "附件",
        "props": {
            "label": "附件",
            "id": "Attachment-V52U77J1",
            "required": false,
            "hide": false,
            "edit": false,
            "multiple": false
        },
        "id": "Attachment-V52U77J1",
        "model": "V52U77J1",
        "inTableField": false,
        "aliasLabel": "attachmentArr",
        "orderStateY": "ok",
        "value": []
    },
    {
        "componentName": "TextField",
        "title": "单行输入框",
        "props": {
            "placeholder": "请输入",
            "label": "备注",
            "id": "TextField-85KHBI5U",
            "required": false,
            "hide": false,
            "edit": false,
            "multiple": false
        },
        "id": "TextField-85KHBI5U",
        "model": "85KHBI5U",
        "inTableField": false,
        "aliasLabel": "remark",
        "autoWrite": {
            "modelId": "",
            "modelName": "",
            "label": "",
            "key": "",
            "primary": ""
        },
        "orderStateY": "ok",
        "value": ""
    }
]
