const arr = [5, 2, 4, 1, 3]

for (let i = 0; i < arr.length - 1; i++) {
    for (let j = i; j < arr.length - 1; j++) {
        if (arr[i] > arr[j + 1]) {
            const cache = arr[j + 1]
            arr[j + 1] = arr[i]
            arr[i] = cache
        }
        console.log(arr)
    }
}

// 52413
// start ==================
// 25413
// 25413
// 15423
// 15423
//
// 14523
// 12543
// 12543
//
// 12453
// 12354
//
// 12345
