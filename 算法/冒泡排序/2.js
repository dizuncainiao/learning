const arr = [5, 2, 4, 1, 3]

for (let i = 0; i < arr.length - 1; i++) {
    for (let j = 0; j < arr.length - 1 - i; j++) {
        if (arr[j] > arr[j + 1]) {
            const cache = arr[j + 1]
            arr[j + 1] = arr[j]
            arr[j] = cache
        }
        console.log(arr)
    }
}

// 52413
// start ==================
// 25413
// 24513
// 24153
// 24135
//
// 24135
// 21435
// 21345
//
// 12345
// 12345
//
// 12345
