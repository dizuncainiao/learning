// 静态作用域
const a = 12

function foo() {
    console.log(a, 'line 4')    // 12 a 始终在创建该函数的作用域取值
}

function test() {
    const a = 8;

    foo()

    function bar() {
        console.log(a, 'line 16')  // 8 a 始终在创建该函数的作用域取值
    }

    return bar
}

const fn = test()
fn()    // 8
