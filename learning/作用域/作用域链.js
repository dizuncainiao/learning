const a = 1
const b = 2
const c = 3
const d = 4

function test() {
    const b = 20
    const c = 30
    const d = 40

    function test2() {
        const c = 300
        const d = 400
        console.log(`a: ${a}, b: ${b}, c: ${c}, d: ${d}`)   //a: 1, b: 20, c: 300, d: 400
    }

    return test2
}

// 在寻找变量时，现在当前的 test2 作用域找，如果没有再在父级作用域 test 中找，再没有就在 全局作用域 中找，这一层一层的作用域被称为：作用域链

test()()
