// 优雅过滤数组中的 falsy 值
const arr = [0, false, null, undefined, NaN, '', 1, 2, 3, 4, 5]
console.log(arr.filter(Boolean))    // [ 1, 2, 3, 4, 5 ]

// 优雅的将数值数组转为字符串数组
const strArr = ['1', '2', '3', '4', '5']
console.log(strArr.map(Number))    // [ 1, 2, 3, 4, 5 ]

// 反之亦可
const strNum = [1, 2, 3, 4, 5]
console.log(strNum.map(String))     // [ '1', '2', '3', '4', '5' ]

// Bad
function returnFnBad(a, b) {
    if (a > b) {
        return a
    } else {
        return b
    }
}

// Good
function returnFnGood(a, b) {
    return a > b ? a : b
}

console.log(returnFnGood(1, 2));    // 2
console.log(returnFnGood(12, 2));   // 12

// 优雅的向下取整
console.log(~~5.1)  // 5
console.log(~~5.9)  // 5
console.log(5.2 | 0)  // 5
console.log(5.2 >> 0)  // 5

// 优雅的将字符串转为数值
console.log(+'123')     // 123
console.log('123')      // '123'

// 优雅的数组去重
let repeatedArr = [1, 2, 3, 4, 5, 5, 4, 3, 2, 1]
console.log([...new Set(repeatedArr)])  // [ 1, 2, 3, 4, 5 ]

// 优雅的数值 1 和 0 互相转换
// Bad
function oneAndZeroSwapBad(a) {
    return a === 0 ? 1 : 0
}

// Good
function oneAndZeroSwapGood(a) {
    return +!a
}

// Good2
function oneAndZeroSwapGood2(a) {
    return Number(!a)
}

// 优雅的转为字符串
console.log(false + '')
console.log(typeof (false + ''))

// 优雅的 if else 判断
// Bad
function judgeFn(type) {
    if (type === 'a') {
        return 'foo'
    } else if (type === 'b') {
        return 'bar'
    }
}

// good
function judgeFnGood(type) {
    const info = {
        a: 'foo',
        b: 'bar'
    }
    return info[type]
}
