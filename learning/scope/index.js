const value = 1

function getVal() {
    console.log(value, new Date().getTime())
}

function test() {
    const value = 2
    getVal()
}

function test2() {
    const value = 3
    return getVal()
}

test()

test2()
