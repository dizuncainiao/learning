var name = 'windowName'

const obj = {
    name: 'objName',
    printName(a, b) {
        console.log(this)
        console.log(this.name, a, b);
    }
}

const target = {
    name: 'targetName'
}

// 将 printName 方法中的 this 替换为 target 对象，并接收参数列表
obj.printName.call(target, 'Hello', 'World')    // targetName Hello World
obj.printName.call(null, 'Hello', 'World')    // windowName Hello World
obj.printName.call(undefined, 'Hello', 'World')    // windowName Hello World
obj.printName.call(false, 'Hello', 'World')    // undefined "Hello" "World"
