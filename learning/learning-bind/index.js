const obj = {
    name: 'foo',
    say: function (a, b) {
        console.log(this.name, a, b)
        console.log('this', this)
    }
}

const obj2 = {name: 'bar'}

const say2 = obj.say
say2()  // undefined undefined undefined

/**
 * bind 方法用来创建一个新的函数，
 * 并将第一个参数作为调用该函数的 this，
 * 并以参数列表的形式将参数传给该函数。
 * 它和 call 的区别就是，call 会直接调用函数，
 * 而 bind 需要手动调用一次
 * @type {any}
 */
const say3 = say2.bind(obj, 1, 2)
say3()  // foo 1 2
const say4 = say2.bind(obj2, 66, 88)
say4()  // bar 66 88
