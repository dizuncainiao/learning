function sleep(delay) {
    return new Promise(resolve => {
        setTimeout(() => {
            resolve()
        }, delay)
    })
}

async function test() {
    console.log('1 秒后打印 666')
    await sleep(1000)
    console.log(666)
}

test()
