function Person(foo, bar) {
    this.foo = foo
    this.bar = bar
}

Person.prototype.getFoo = function () {
    console.log(this.foo)
}

function _new() {
    // 创建一个空对象
    const obj = Object.create(null)
    // 构造函数
    const Constructor2 = arguments[0]
    // 摒除 构造函数 外的参数
    const args = Array.prototype.slice.call(arguments, 1)
    // 给 obj 对象写入基本属性
    Constructor2.apply(obj, args)
    // 将 obj 的 __proto__ 属性指向构造函数的原型
    Object.defineProperty(obj, '__proto__', {
        value: Constructor2.prototype
    })
    return obj
}

const p = _new(Person, 'foo', 'bar')
console.log(p);
