Function.prototype.apply2 = function (context, arr) {
    // 这里使用 Object.create(null) 创建一个纯净对象
    context = context || Object.create(null)
    context.fn = this

    let result
    if (!arr) {
        result = context.fn()
    } else {
        let args = []
        for (let i = 0; i < arr.length; i++) {
            args.push('arr[' + i + ']')
        }
        result = eval('context.fn(' + args + ')')
    }
    delete context.fn
    return result
}

const obj = {
    foo: 'objFoo',
    say: function (a, b) {
        console.log(this.foo, a, b)
    }
}

const obj2 = {foo: 'obj2Foo'}

obj.say.apply2(obj2, [1, 2])
obj.say.apply2(false, [1, 2])
