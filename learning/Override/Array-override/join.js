Array.prototype._join = function (separator = ',') {
    let result = ''
    for (let i = 0; i < this.length - 1; i++) {
        result += this[i] + separator
    }
    result = result + this[this.length - 1]
    return result
}

const arr = [1, 2, 3, 4, 5]
console.log(arr._join(' -- '))
console.log(arr.join())
