Array.prototype._push = function () {
    for (let k in arguments) {
        this[this.length] = arguments[k]
    }
}

const arr = [1, 2, 3, 4, 5]

Array.prototype._map = function (callback) {
    if (!this.length) return undefined;
    const result = []

    for (let i = 0; i < this.length; i++) {
        result._push(callback(this[i], i, this))
    }

    return result
};

console.log(arr._map((item, index, arr) => item * 2));
