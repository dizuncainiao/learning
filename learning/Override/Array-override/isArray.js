Array._isArray = function (arr) {
    return arr instanceof Array
}

console.log(Array._isArray(true));
console.log(Array._isArray([]));
