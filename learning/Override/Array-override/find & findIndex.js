Array.prototype._find = function (callback) {
    if (!this.length) return undefined;

    for (let i = 0; i < this.length; i++) {
        if (callback(this[i], i, this)) {
            return this[i]
        }
    }
};

Array.prototype._findIndex = function (callback) {
    if (!this.length) return undefined;

    for (let i = 0; i < this.length; i++) {
        if (callback(this[i], i, this)) {
            return i
        }
    }

    return -1
};


console.log([1, 2, 3, 4]._find(item => item > 0));
console.log([1, 2, 3, 4]._findIndex(item => item > 0));
console.log([1, 2, 3, 4]._findIndex(item => item > 8));
