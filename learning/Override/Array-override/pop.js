Array.prototype._pop = function () {
    const lastItem = this[this.length - 1]
    this.length = this.length - 1
    return lastItem
};

const arr = [1, 2, 3, 4, 5]

console.log(arr._pop(), arr);
