const arr = [1, 2, 3, 4, 5]

Array.prototype._forEach = function (callback) {
    if (!this.length) return undefined;

    for (let i = 0; i < this.length; i++) {
        callback(this[i], i, this)
    }

    return undefined
};

arr._forEach((item, index, arr) => {
    console.log(item, index, arr);
})
