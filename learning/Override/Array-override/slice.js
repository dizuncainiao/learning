const arr = [1, 2, 3, 4, 5]

// console.log(arr.slice())    // [1, 2, 3, 4, 5]  arr.slice() => arr.slice(0, this.length)

Array.prototype._push = function () {
    for (let k in arguments) {
        this[this.length] = arguments[k]
    }
}

Array.prototype._slice = function (start = 0, end = this.length) {
    const result = []
    for (let i = start; i < end; i++) {
        result._push(this[i])
    }
    return result
}

console.log(arr._slice())       // [ 1, 2, 3, 4, 5 ]
console.log(arr._slice(1, 3))   // [ 2, 3 ]
console.log(arr.slice(1, 3))   // [ 2, 3 ]
