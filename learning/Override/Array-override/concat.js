Array.prototype._push = function () {
    for (let k in arguments) {
        this[this.length] = arguments[k]
    }
}

Array.prototype._map = function (callback) {
    if (!this.length) return undefined;
    const result = []

    for (let i = 0; i < this.length; i++) {
        result._push(callback(this[i], i, this))
    }

    return result
};

Array.prototype._concat = function () {
    let result = this

    for (let k in arguments) {
        for (let i = 0; i < arguments[k].length; i++) {
            result._push(arguments[k][i])
        }
    }
    return result
}

const arr = [1, 2, 3];
const arr2 = [4, 5, 6];

console.log([-1, 0]._concat(arr, arr2));
