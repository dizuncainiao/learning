Array.prototype._reduce = function (callback) {
    if (!this.length) return undefined;
    let total
    for (let i = 0; i < this.length; i++) {
        if (i === 0) {
            total = this[i]
        } else {
            total = callback(total, this[i])
        }

    }
    return total
};


console.log([1, 2, 3, 4, 5]._reduce((total, num) => (total + num)));
