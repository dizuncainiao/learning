Array.prototype._push = function () {
    for (let k in arguments) {
        this[this.length] = arguments[k]
    }
}

Array.prototype._filter = function (callback) {
    if (!this.length) return undefined;

    const result = []
    for (let i = 0; i < this.length; i++) {
        if (callback(this[i], i, this)) {
            result._push(this[i])
        }
    }
    return result
};


console.log([1, 2, 3, 4, 5]._filter(item => item > 1));
