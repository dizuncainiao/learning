Array.prototype._some = function (callback) {
    if (!this.length) return undefined;

    for (let i = 0; i < this.length; i++) {
        if (callback(this[i], i, this)) {
            return true
        }
    }

    return false
};

console.log([1, 2, 3, 4]._some(item => item > 2));  // true
console.log([1, 2, 3, 4]._some(item => item > 4));  // false
