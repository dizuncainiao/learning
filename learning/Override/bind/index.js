Function.prototype.bind2 = function () {
    const
        // 当前的函数
        fn = this,
        // 传入的 this 对象
        context = arguments[0] ? arguments[0] : getContext(arguments[0]),
        // 获取除 context 外的其他参数
        args = Array.prototype.slice.call(arguments, 1);

    function getContext(target) {
        return (target === null || target === undefined) ? window : undefined
    }

    // typeof Function.prototype.bind === 'function'
    return function () {
        // 这里的 return 是为了返回被执行函数的返回值
        return fn.apply(context, args)
    }

}

const obj = {
    name: 'objName',
    say: function (a, b) {
        console.log(this.name, a, b)
        return 666
    }
}
const say2 = obj.say
const say3 = say2.bind2(obj, 1, 2)
console.log(say3())
