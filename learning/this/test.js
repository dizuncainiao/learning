function Person() {
    console.log(this, 'line 2')
}

Person.prototype.getThis = function () {
    console.log(this, 'line 6')
}

const p = new Person()  // Person {}

Person.prototype.getThis()  // Person { getThis: [Function (anonymous)] }
p.getThis() // Person {}

