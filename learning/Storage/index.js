class StorageSelf {

    constructor() {
        this.length = 0
    }

    _setLength() {
        this.length = Object.keys(this).length - 1
    }

    setItem(key, value) {
        this[key] = JSON.stringify(value)
        this._setLength()
    }

    getItem(key) {
        return this[key]
    }

    removeItem(key) {
        delete this[key]
        this._setLength()
    }

    clear() {
        for (let k in this) {
            delete this[k]
        }
        this.length = 0
    }
}

const storage = new StorageSelf()

storage.setItem('foo', 'foo')
storage.setItem('bar', 'bar')
console.log(storage);
