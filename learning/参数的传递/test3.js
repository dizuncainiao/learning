let obj = { foo: 'foo' }

function test (_obj) {
    _obj = 'foo'
    console.log(_obj);
}

test(obj)   // foo
console.log(obj);   // { foo: 'foo' }
