const obj = {foo: 'foo'}

function test(_obj) {
    _obj.foo = 'bar'
    console.log(_obj);
}
test(obj)   // { foo: 'bar' }
console.log(obj);   // { foo: 'bar' }
