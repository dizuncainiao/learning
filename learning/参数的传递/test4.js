let obj = {foo: 'foo'}

function test(_obj) {
    _obj.bar = 'bar'    // 仍然能修改到 obj
    _obj = {bar: 'bar'} //  _obj 被分配到了一个新的内存地址
    _obj.bar2 = 'bar2'  // 不会修改到 obj
    console.log(_obj);
}

test(obj)   // { bar: 'bar', bar2: 'bar2' }
console.log(obj);   // { foo: 'foo', bar: 'bar' }
