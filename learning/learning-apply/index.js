const obj = {
    name: 'foo',
    printName() {
        console.log(this.name);
        console.log(arguments);
    }
}

const target = {
    name: 'bar'
}

// 将 printName 方法中的 this 替换为 target 对象，并接收数组作为参数
obj.printName.apply(target, ['Hello', 'World'])    // bar
