function counter() {
    let n = 0

    function add() {    //  add函数中引用了父级作用域的变量 n
        n++
        console.log(n);
    }

    return add  // counter函数将 add函数 作为返回值
}

const result = counter()    // result 被赋值为 add函数，而 add函数 中依赖 n，所有 n 一直保存在内存中
result()    // 1
result()    // 2
result()    // 3
